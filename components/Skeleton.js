import React from "react";
import { View } from "react-native";
import { WIDTH } from "../theme";

const Skeleton = () => (
  <View
    style={{
      padding: 20,
      height: 150,
      width: WIDTH,
      marginBottom: 5,
      backgroundColor: "rgba(0,0,0,0.1)",
    }}
  >
    <View style={{ width: 110, height: 110, backgroundColor: "rgba(0,0,0,.1)"}} />
  </View>
);

export default Skeleton;
