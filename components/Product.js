import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { dark_blue } from "../theme";
import { SimpleLineIcons } from "@expo/vector-icons";

const Product = ({ item }) => (
  <TouchableOpacity style={styles.card} onPress={() => alert("Added to cart")}>
    <>
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Image style={{ height: 74, width: 90 }} source={item.image} />

        <View style={{ flex: 1 }}>
          <Text style={styles.label}>Original</Text>
          <Text style={styles.label}>Selling</Text>
          <Text style={styles.daysAgo}>5 days ago</Text>
        </View>

        <View style={{ flex: 1 }}>
          <Text style={styles.tag}>{item.originalPrice}</Text>
          <Text style={[styles.tag, { color: "#2F9752" }]}>
            {item.sellingPrice}
          </Text>
        </View>

        <View style={{ backgroundColor: "#EEEEEE", flex: 1 }}>
          <Text style={styles.details}>{item.brand}</Text>
          <Text style={styles.details}>{item.type}</Text>
          <Text style={styles.details}>{item.category}</Text>
        </View>

        <SimpleLineIcons
          style={{ paddingLeft: 8 }}
          name="arrow-right"
          size={24}
          color="black"
        />
      </View>
      <Text style={styles.itemName}>{item.name}</Text>
    </>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  card: {
    padding: 15,
    borderBottomWidth: .4,
    borderColor: "gray",
  },
  label: {
    backgroundColor: dark_blue,
    color: "#fff",
    fontSize: 14,
    padding: 5,
  },
  daysAgo: {
    fontSize: 8,
    color: "gray",
  },
  tag: {
    fontSize: 22,
    padding: 4,
    marginTop: -4,
  },
  details: {
    fontSize: 8,
    paddingVertical: 5,
    paddingHorizontal: 8,
  },
  itemName: {
    fontSize: 18
  }
});

export default Product;
