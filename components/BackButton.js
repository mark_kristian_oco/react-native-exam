import React from "react";
import { TouchableOpacity } from "react-native";
import { SimpleLineIcons } from "@expo/vector-icons";
import { useNavigation } from "@react-navigation/native";

const BackButton = () => {

  const navigation = useNavigation();

  return (
    <TouchableOpacity style={{ paddingLeft: 8 }} onPress={() => navigation.goBack()}>
      <SimpleLineIcons name="arrow-left" size={24} color="#fff" />
    </TouchableOpacity>
  );
};

export default BackButton;
