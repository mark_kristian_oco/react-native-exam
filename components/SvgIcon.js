import React from "react";
import { View } from "react-native";
import Svg, { Path } from "react-native-svg";
import { light_blue } from "../theme";

export const LoginSvgIcon = () => (
  <View>
    <Svg
      width={169}
      height={169}
      viewBox="0 0 169.034 169.075"
      fill={"none"}
      xmlns="http://www.w3.org/2000/svg"
    >
      <Path
        transform="translate(-112.792 -47.455)"
        d={path1}
        fill={light_blue}
      />
      <Path transform="translate(-115.176 -44.507)" d={path2} fill="#000" />
      <Path
        transform="translate(-112.869 -43.618)"
        d={path3}
        fill={light_blue}
      />
      <Path transform="translate(-116.309 -43.039)" d={path4} fill="#000" />
      <Path transform="translate(-111.672 -42.42)" d={path5} />
    </Svg>
  </View>
);

const path1 = "M170.209,178.284,137.621,191.7l13.415-32.588Z";
const path2 =
  "M228.315,90.78l-33.436,33.44a6.829,6.829,0,1,0,9.659,9.658l33.434-33.434Z";
const path3 =
  "M241.839,116.891l-.262-.234A45.574,45.574,0,1,1,209,83.977l9.749-9.743a58.024,58.024,0,1,0,32.717,33.032Z";
const path4 =
  "M248.26,80.163l-1.169-23.389L219.132,84.742l1.162,23.386,23.386,1.165,27.968-27.965Z";
const path5 =
  "M196.208,211.5a84.536,84.536,0,1,1,59.779-24.757A83.988,83.988,0,0,1,196.208,211.5Z";
