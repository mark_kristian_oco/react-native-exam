//Themes

import { Dimensions } from "react-native";
  
 export const WIDTH = Dimensions.get("window").width;
 export const dark_blue = "#2987e1";
 export const light_blue = "#47b2d9";