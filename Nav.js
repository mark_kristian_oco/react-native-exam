import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import ListProduct from "./screens/ListProduct";
import Login from "./screens/Login";
import { dark_blue } from "./theme";
import BackButton from "./components/BackButton";

const Stack = createStackNavigator();

const Nav = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          options={{ headerShown: false }}
          name="LoginPage"
          component={Login}
        />
        <Stack.Screen
          options={{
            title: "Test Product",
            headerTintColor: "#fff",
            headerStyle: {
              backgroundColor: dark_blue,
            },
            headerLeft: () => <BackButton />,
          }}
          name="ListProductPage"
          component={ListProduct}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Nav;
