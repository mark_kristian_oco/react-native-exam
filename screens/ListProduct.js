import React from "react";
import { useEffect } from "react";
import { View, StyleSheet, FlatList } from "react-native";
import { useState } from "react/cjs/react.development";
import { getProducts } from "../api/RestApi";
import Product from "../components/Product";
import Skeleton from "../components/Skeleton";
import { dark_blue } from "../theme";


const ListProduct = () => {
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
      getProducts().then((res) => {
        setProducts(res);
        setIsLoading(false);
      }).catch( error => {
        setProducts([...products]);
        setIsLoading(false);
        alert(`Oops! Something went wrong. :( ${error.message}`);
      })
  }, []);

  return (
    <View style={styles.container}>
      {isLoading ? 
        [...Array(10)].map((_,index) => (
          <Skeleton key={index} />
        ))
      :
      <FlatList
        showsVerticalScrollIndicator={false}
        data={products}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }, index) => <Product item={item} />}
      />
      }
      
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  label: {
    backgroundColor: dark_blue,
    color: "#fff",
    fontSize: 14,
    padding: 5,
  },
  daysAgo: {
    fontSize: 8,
    color: "gray",
  },
  tag: {
    fontSize: 20,
    padding: 4,
    marginTop: -4,
  },
  details: {
    fontSize: 8,
    paddingVertical: 5,
    paddingHorizontal: 8,
  },
});

export default ListProduct;
