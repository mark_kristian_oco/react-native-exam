import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  SafeAreaView,
  ActivityIndicator,
} from "react-native";
import { LoginSvgIcon } from "../components/SvgIcon";
import { dark_blue } from "../theme";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { login } from "../api/RestApi";

const Login = ({ navigation }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  //Validate user login
  const handleLogin = () => {
    setIsLoading(true);
    login(username, password).then((res) => {
      if (res) {
        navigation.navigate("ListProductPage");
        setUsername("");
        setPassword("");
      } else {
        alert("Oops! Incorrect username or parssword.");
        setPassword("");
      }
      setIsLoading(false);
    }).catch( error => {
      alert(`Oops! Something went wrong. :( ${error.message}`);
      setPassword("");
      setIsLoading(false);
    })
  };

  return (
    <KeyboardAwareScrollView contentContainerStyle={{ flex: 1 }}>
      <SafeAreaView style={styles.container}>
        <TouchableWithoutFeedback
          style={{ flex: 1 }}
          onPress={() => Keyboard.dismiss()}
        >
          <View style={styles.container}>
            <View style={styles.logo}>
              <LoginSvgIcon />
            </View>

            <Text style={styles.title}>Login</Text>
            <Text style={styles.muted}>Please login to your account.</Text>

            <View>
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                value={username}
                onChangeText={setUsername}
                style={styles.input}
                placeholder="Username"
                autoFocus
              />
              <TextInput
                autoCapitalize="none"
                autoCorrect={false}
                value={password}
                onChangeText={setPassword}
                style={styles.input}
                placeholder="Password"
                secureTextEntry
              />
            </View>

            <View style={styles.btnContainer}>
              <TouchableOpacity style={styles.btn} onPress={handleLogin}>
                <Text style={styles.btnText}>
                  {isLoading ? "PLEASE WAIT" : "LOGIN"}
                </Text>
                {isLoading && <ActivityIndicator color="#fff" size={24} />}
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    backgroundColor: "#fff",
    paddingHorizontal: 50,
    paddingVertical: 25
  },
  title: {
    fontSize: 34,
    fontWeight: "800",
    marginTop: 16,
  },
  muted: {
    marginTop: 17,
    marginBottom: 25,
    fontSize: 18,
    color: "#1D2226",
    opacity: 0.6,
  },
  input: {
    marginTop: 16,
    height: 60,
    borderBottomColor: "gray",
    borderBottomWidth: StyleSheet.hairlineWidth,
    fontSize: 14,
  },
  btn: {
    marginTop: 20,
    backgroundColor: dark_blue,
    borderRadius: 50,
    height: 52,
    justifyContent: "center",
    flexDirection: "row",
  },
  btnText: {
    color: "#fff",
    alignSelf: "center",
    fontWeight: "700",
    marginRight: 8,
  },
  logo: {
    alignSelf: "center",
  },
  btnContainer: {
    flex: 1,
    justifyContent: "flex-end",
  },
});

export default Login;
